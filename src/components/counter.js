const Counter = (props) => {
    const { index, updateCounterValue, counter } = props

    const increase = () => {
        const newCounter = counter + 1;
        updateCounterValue(newCounter, index)
    }

    const decrease = () => {
        const newCounter = counter - 1;
        updateCounterValue(newCounter, index)
    }

    return (
        <div className='counter'>
            <button onClick={increase}>+</button>
            <h3>{counter}</h3>
            <button onClick={decrease}>-</button>
        </div>
    )
}

export default Counter;