import Counter from "./Counter";
const CounterGroup = (props) => {

    const { counterList, setCounterList } = props;
  
    const updateCounterValue = (newValue, index) => {
      const list = counterList.map((counter, itemIndex) => {
        if (itemIndex === index) {
          return newValue;
        }
        return counter;
      })
      setCounterList(list)
    }
    
    return (
        <div>
        {counterList.map((counter, index) => (
          <Counter key={index} index={index} counter={counter}
          updateCounterValue={(value) => {updateCounterValue(value, index)}}/>
        ))}
      </div>
    )
  }




export default CounterGroup;