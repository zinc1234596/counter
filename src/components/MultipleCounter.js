import { useState } from "react";
import CounterGroup from "./CounterGroup";
import CounterSizeGenerator from "./CounterSizeGenerator";
import CounterSum from "./CounterSum";
const MultipleCounter = () => {

    const [counterList, setCounterList] = useState([]);

    return (
        <div>
            <CounterSizeGenerator
                counterListLength={counterList.length}
                setCounterList={setCounterList}
            />

            <CounterSum
                counterList={counterList}
            />

            <CounterGroup
                counterList={counterList}
                setCounterList={setCounterList}
            />
        </div>
    );
}

export default MultipleCounter;