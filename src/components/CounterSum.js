const CounterSum = ({counterList}) => {

    const sum = counterList.reduce((total, counter) => total + counter, 0);
  
    return (
      <div>
        Sum: {sum}
      </div>
    )
  
  }

export default CounterSum;