const CounterSizeGenerator = (props) => {
    const {setCounterList} = props;
  
    const updateSize = (event) => {
      const length = event.target.value;
      const array = Array.from({length: length}, (_, index) => 0);
      setCounterList(array)
    }
  
    return (
      <div className='generator'>
        <p style={{display:'inline'}}>size:</p>
        <input type="number" onChange={updateSize}/>
      </div>
    )
  }

export default CounterSizeGenerator;